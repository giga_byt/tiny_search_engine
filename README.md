# CS50 Tiny Search Engine

Bill Tang - Summer 2017

To build, run `make`.

To clean up, run `make clean`.

The CS50 playground is in 
http://old-www.cs.dartmouth.edu/~cs50/data/tse/
