# Querier
## User Interface
The queriers’s first interface with the user is on the command-line; 
it must always have two arguments.
`querier pageDir indexFile`
For example:
`./querier wikitest input.txt`

Then the querier launches, and the user makes search queries to the querier
search engine on the indexFile. To exit, use `ctrl-d`.
## Inputs and Outputs
Input: the inputs are command-line parameters and search queries; see the User 
Interface above.
Output: none.

## Functional decomposition into modules
1. main, which parses arguments and initializes other modules
2. queryUI, which is the constantly-looping interface that accepts queries and uses
other modules to process the query
3. queryParser, which breaks down the query into arrays and logical structures
4. searcher, which, looking at the parsed query obtains lists of documents and scores from the index and 
compares them, finally assembling a list of matched results with scores and prints them.
4. And some helper modules that provide data structures:

index, which itself uses a great deal of hashtables, sets, etc.
intArray, wordArray, and documentScoreArray which are effectively arrays but with
size variables to ease code-writing.
documentScore, a struct that pairs off documents and their scores - effectively 
a counter, but much more flexible with its functionality.

## Pseudocode
The querier runs as follows:
1. read in args, do errorchecking
2. load in the index
3. begin looping the queryUI on the command line:
    1. accepts queries and error-checks them
    2. passes properly formatted queries to a parser, which turns the query into an array
    3. another parser breaks the wordArray into logical structures of andSequences and ors,
    represented by sets.
    4. the searcher finds the intersections of each andSequence, obtaining a list of documents
    that are common between members of an andsequence.
    5. the searcher goes through this list and calculates the score for each 
    document over the whole query, making another list of paired doc-scores for the query.
    6. the searcher finally takes this list, sorts it, then prints it out in order.
    7. this iteration of the queryUI is finished. free everything, reloop.
4. The program concludes. Free everything.

## Dataflow through modules
1. main, which parses parameters and starts the queryUI
2. queryUI gets queries from user and passes them to queryParser
3. queryParser, takes the queries and processes them into a computer-readable form
4. searcher, takes the processed queries, retrieves data for each andSequence, compares,
and prints out the result of the search.

## Major Data Structures
The index, a hashtable mapping *words* to *(docNum, count)*.
The wordArray, an array of strings that contains information on size.
The intArray, an array of ints that contains information on its size.
The documentScore, a pair of document and score.
The documentScoreArray, an array of the above with information on its size.
The counter and set, both things mapping keys to values - in this case, documents to scores
and words to counters, respectively.

## Testing plan
*Unit Testing.* Each individual module in it was tested on a junk dir and junk 
files, which no longer exist, and confirmed to work.

*Integration Testing.* Tested the program with various bad command-line args,
bad dirs, non-existent files, which can be done simply via command line. Finally,
it is tested on actual output from the crawler and indexer, and searched every combination
that could be thought of under the sun to test error-checking built into the program.