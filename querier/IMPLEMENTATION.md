# Implementation
## Querier
### Detailed Pseudo-Code

1. parse args into variables
2. load index file into searchIndex
3. begin the loop that asks the user for queries
    1. read query into string
    2. errorcheck the string for formatting
    3. parse the query into a words_t queryWordArray
    4. syntaxcheck the array (e.g. having AND/OR adjacency, or starting with AND/OR)
    reloop if fails.
    5. print the query for confirmation
    6. calculate the number of andSequences by parsing the queryWordArray
    7. create an array of sets (searchQueries[n]) containing counters, one for each andSequence.
    8. within each set/andSequence, get a list of documents that are common to all
    search terms, and read the list of integer document names into queryLists[n], one for 
    each andSequence.
    9. create a docScoreArray containing docScores in preparation for sorting and printing
    10. create a counters unfilteredDocScores for data storage.
    11. iterate through the queryLists (containing the list of common documents) and
    obtains a score for each document over the whole query; taking the min score of the words
    in each individual andSequence and adding all andSequences' scores together. fill
    in the unfilteredDocScores with the scores for each document. (N.B. there will 
    be doubling as a result of this method, but the counters architecture takes care of this).
    12. read the unfilteredDocScores, which by virtue of being in a counter have filtered
    doubles, and copy the docNum-Score pairs into the docScores array.
    13. sort the docScores array by score, highest-to-lowest.
    14. print out the scores, docnums, and the URLs, which are fetched from the files in
    the directory specified in the commandline.
    15. free everything local to this iteration of the querier, and loop until user ends loop.
4. free everything, exit.

### querier.c API

```
/*
 * The main function is called via commandline with command-line args. It makes
 * use of a large variety of module functions and structs, all opaque to the user,
 * which enable it to read in queries and output results for each search.
 */
int main(int argc, char *argv[]);
```
### Data Structures
The index, a hashtable mapping words to (docNum, count).

The wordArray, an array of strings that contains information on size.

The intArray, an array of ints that contains information on its size.

The documentScore, a pair of document and score.

The documentScoreArray, an array of the above with information on its size.

The counter and set, both things mapping keys to values - in this case, documents to scores
and words to counters, respectively.
### Error Handling
The program catches command-line errors of all types, bad parameter errors resulting
from bad arguments, and almost all user input failures including bad dirs, bad search terms,
and empty strings.

It does very little error-checking on the contents of the files that the querier
relies on, such as the index file and the directory full of webpages; this is permitted
in the assumptions page.

### Resource Management
All things are malloc'ed and freed that need to be. Every iteration of the querier
mallocs and frees its local variables: every readlinep and readwordp is freed as well, 
and every `fopen`, there is an `fclose`.

### Persistent Storage
None. It requires a dir full of crawler output, and an index file to start, however.