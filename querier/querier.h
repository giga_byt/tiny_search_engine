/* 
 * querier - a program that reads in search queries, and searches the index 
 * produced by TSE indexer and TSE crawler to find search terms.
 *
 * usage - ./querier pageDirectory indexFilename
 * pageDirectory is the name of the folder created by crawler
 * indexFileName is the name of a file output by indexer
 * Assumptions: none yet
 * Bill Tang - CS50, August 2017
 * 
 */
 
int main(int argc, char *argv[]);
