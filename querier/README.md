# Compilation
To compile, use `make`.
To clean, use `make clean`.
To conduct a test, I programmed `make test` into the makefile, which will
conduct a test of the `querier` functionality on the output of `make wikitest`
in the indexer directory.

# Usage
To use the functions, simply type `make` and then use:
`./querier pagedir indexfile`

# Assumptions
None made outside of the ones permitted in the REQUIREMENTS doc, which were
quite generous.