/* 
 * querier - a program that reads in search queries, and searches the index 
 * produced by TSE indexer and TSE crawler to find search terms.
 *
 * usage - ./querier pageDirectory indexFilename
 * pageDirectory is the name of the folder created by crawler
 * indexFileName is the name of a file output by indexer
 * Assumptions: none yet
 * Bill Tang - CS50, August 2017
 * 
 */
 
#define _USAGE "./querier pageDirectory indexFilename"
#define _EXPECTED_MAX_RESULTS 3000
#define _QUERYSIZE 200
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <index.h>
#include <file.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <word.h>
#include <set.h>
#include <counters.h>
#include <math.h>


//[INCLUDES GO HERE]

typedef struct wordArray {
  char** words;
  int size;
} words_t;

typedef struct intArray {
  int* ints;
  int size;
  int firstEmptyPos;
} int_t;

typedef struct documentScore {
  int score;
  int docNum;
} docScore_t;

typedef struct documentScoreArray{
  docScore_t **array;
  int size;
} docScoreArray_t;

//[HELPER FUNCS GO HERE]
int directoryIsNonEmpty(char *dirname);
char *askInput();
bool isOnlyAlpha(char* str);
bool syntaxCheck(words_t *query);
words_t *parseQuery(char* query, int arraySize);
int numAndSequences(words_t *array);
set_t **parseAndSequences(words_t *inputArray, index_t *index);
int_t *intersectionOfSet(set_t **sets, int setNum, words_t *inputArray);
void addKey(void* keyList, const int key, int count);
int getScoreAndSeq(int setNum, int docNum, words_t *inputArray, index_t *index);

words_t *newWordArray(char** array, int size);
void deleteWordArray(words_t *array);
void printWordArray(words_t *array);

int_t *newIntArray();
void addIntToArray(int_t *array, int x);
void expandArray(int_t *array);
int getVal(int_t *array, int k);
void setInt(int_t *array, int index, int val);
void deleteIntArray(int_t *array);

docScore_t *newDocScore(int score, const int docNum);
docScoreArray_t *newDocScoreArray(int expectedSize);
void addDocToArray(void *array, const int docNum, int docScore);
int compareDocScore(const void *docOne, const void *docTwo);

int main(int argc, char *argv[]){
  //first, process args and error-check
  if(argc != 3){
    printf("Usage:%s\n", _USAGE);
    return 1;
  }
  char *pageDir = argv[1];
  char *fileName = argv[2];
  if(directoryIsNonEmpty(pageDir) != 0){
    printf("Usage:%s || PageDir must contain files and exist\n", _USAGE);
    return 1;
  }
  if(access(fileName, F_OK) == -1){
    printf("Usage:%s || fileName must point to valid file\n", _USAGE);
    return 1;
  }
  
  //load index from file
  index_t *searchIndex = load_index_from_file(fileName, 1000);
  char* inputQuery;
  int numSequences;
  while((inputQuery = askInput()) != NULL){
    
    //read in the Query, break into an array, and errorcheck
    if(!isOnlyAlpha(inputQuery)){
      printf("invalid input - letters and spaces only\n");
      free(inputQuery);
      continue;
    }
    words_t *queryWordArray = parseQuery(inputQuery, _QUERYSIZE);
    if(queryWordArray == NULL){
      free(inputQuery);
      continue;
    }
    if(!syntaxCheck(queryWordArray)){
      free(inputQuery);
      deleteWordArray(queryWordArray);
      continue;
    }
    printWordArray(queryWordArray);
    //at this point,  queryWordArray contains the whole query in an array
    numSequences = numAndSequences(queryWordArray);
    set_t **searchQueries = parseAndSequences(queryWordArray, searchIndex);
    //searchQueries have been parsed into sets.
    
    int_t **queryLists = allocate(sizeof(int_t*) * numSequences);
    //compare
    for(int i = 0; i < numSequences; i++){
      if(searchQueries[i] != NULL)
        queryLists[i] = intersectionOfSet(searchQueries, i, queryWordArray);
    }
    int numDocs = 0;
    docScoreArray_t *docScores = newDocScoreArray(_EXPECTED_MAX_RESULTS);
    counters_t *unfilteredDocScores = counters_new();
    for(int andSeq = 0; andSeq < numSequences; andSeq++){
      if(queryLists[andSeq] != NULL){
        for(int idx = 0; idx < queryLists[andSeq]->firstEmptyPos; idx++){
          int* ints = queryLists[andSeq]->ints;
          int docNum = ints[idx];
          if(docNum != -1){
            int currScoreForDoc = 0;
            for(int i = 0; i < numSequences; i++){
              currScoreForDoc += getScoreAndSeq(i, ints[idx], queryWordArray, searchIndex);
            }
            counters_set(unfilteredDocScores, docNum, currScoreForDoc);
          }
        }
      }
    }
    counters_iterate(unfilteredDocScores, docScores, addDocToArray);
    docScore_t** docArray = docScores->array;
    qsort(docArray, docScores->size, sizeof(docScore_t), compareDocScore);
    printf("Matches %d documents (ranked):\n", docScores->size);
    char* URL;
    for(int i = 0; i < docScores->size; i++){
      char cacheFileName[80];
      sprintf(cacheFileName, "%s/%d", pageDir, docArray[i]->docNum);
      FILE *htmlFile = fopen(cacheFileName, "r");
      URL = readlinep(htmlFile);
      printf("Doc: %4d| score: %5d| URL: %s\n", docArray[i]->docNum, docArray[i]->score, URL);
      fclose(htmlFile);
      free(URL);
      memset(cacheFileName,0,strlen(cacheFileName));
    }
    //free andSequences set;
    for(int i = 0; i < numSequences; i++){
      if(searchQueries[i] != NULL){
        set_delete(searchQueries[i], NULL);
        deleteIntArray(queryLists[i]);
      }
    }
    for(int i = 0; i < docScores->size; i++){
      free(docArray[i]);
    }
    free(docArray);
    free(docScores);
    counters_delete(unfilteredDocScores);
    free(queryLists);
    free(searchQueries);
    free(inputQuery);
    deleteWordArray(queryWordArray);
  }

  index_delete(searchIndex);
}

/*
 * Helper Method: Special Formatted Print for counters.
 */

//void queryPrint(void *arg, const int key, int count){
//  printf("score: %d|doc: %d\n| %s", count, key, arg);
//}


/*
 * Helper Method: gets the score for a single andSequence for a given document number.
 */

int getScoreAndSeq(int setNum, int docNum, words_t *inputArray, index_t *index){
  int score = -1;
  char** words = inputArray->words;
  int queryNum = 0;
  int startIndex = 0;
  while(queryNum != setNum){
    if(strcmp(words[startIndex], "or") == 0){
        queryNum++;
        startIndex++;
    }else{
      startIndex++;
    }
  }
  int currIndex = startIndex;
  int endIndex = startIndex;
  
  while(endIndex < inputArray->size && strcmp(words[endIndex], "or") != 0){
    endIndex++;
  }
  for(int i = startIndex; i < endIndex; i++){
    if(strcmp(words[i], "and") == 0)
      continue;
    if(index_get_count(index, words[i], docNum) < score || score == -1){
      score = index_get_count(index, words[i], docNum);
    }
  }
  return score;
} 


/*
 * Helper function; Given a set of counters, compares them by key.
 * This function returns an integer array containing all keys that are common
 * between all counters in the set - a value of -1 indicates a key that was non-common
 * and thus deleted.
 * Don't forget to free.
 */

int_t *intersectionOfSet(set_t **sets, int setNum, words_t *inputArray){
  set_t *curr = sets[setNum];
  char** words = inputArray->words;
  int queryNum = 0;
  int startIndex = 0;
  while(queryNum != setNum){
    if(strcmp(words[startIndex], "or") == 0){
      startIndex++;
      queryNum++;
    }else{ 
      startIndex++;
    }
  }
  int currIndex = startIndex;
  int endIndex = startIndex;
  while(endIndex < inputArray->size && strcmp(words[endIndex], "or") != 0){
    endIndex++;
  }
  int_t *list = newIntArray();
  counters_t *first = set_find(curr, words[startIndex]);
  counters_t *currCounter;
  counters_iterate(first, list, addKey);
  for(int i = 0; i < list->firstEmptyPos; i++){
    int docNum = getVal(list, i);
    for(int k = startIndex; k < endIndex; k++){
      if(strcmp(words[k], "and") == 0)
        continue;
      currCounter = set_find(curr, words[k]);
      if(counters_get(currCounter, docNum) == 0){
        setInt(list, i, -1);
      }
    }
  }
  return list;
}

/*
 * Helper Helper Function; this is a function meant to be used in counters_iterate,
 * and reads into the provided integer array a list of keys in the counter.
 */
 
void addKey(void *keyList, const int key, int count){
  int_t *list = keyList;
  addIntToArray(list, key);
}
 
/*
 * Helper function; given a query, parses out sets of AndSequences, containing counters
 * for each word, loaded from the index.
 * Returns a pointer to an array of sets containing counters, one for each word in an andSequence. 
 * If a particular AndSequence has a word that does not appear in the index, its pointer 
 * will be NULL - i.e. the query is not found.
 * MEMORY NOTE: only the andSequences array itself and the sets need to be freed;
 * the items themselves reside in the index and therefore should not be freed at 
 * any point - leave it to the index itself to free.
 */

set_t **parseAndSequences(words_t *inputArray, index_t *index){
  int numSequences = numAndSequences(inputArray);
  char** tmp = inputArray->words;
  set_t **andSequences = allocate(sizeof(set_t*) * numSequences);
  bool queryNotFound[numSequences];
  for(int x = 0; x < numSequences; x++){
    andSequences[x] = set_new();
    queryNotFound[x] = false;
  }
  int currSequence = 0;

  
  for(int i = 0; i < inputArray->size; i++){
    if(strcmp(tmp[i], "or") == 0){
      currSequence++;
      continue;
    }
    counters_t *k = index_find_key(index, tmp[i]);
    if(k == NULL){
      queryNotFound[currSequence] = true;
      continue;
    }
    set_insert(andSequences[currSequence], tmp[i], k);
  }
  
  for(int i = 0; i < numSequences; i++){
    if(queryNotFound[i]){
      printf("No Documents Match For AndSequence %d\n", i+1);
      set_delete(andSequences[i], NULL);
      andSequences[i] = NULL;
    }
  }
  
  return andSequences;
}
 
/*
 * Helper function; calculates number of andsequences in the query (by using OR) as
 * a delimiter.
 */
 
int numAndSequences(words_t *array){
  int num = 1;
  char** tmp = array->words;
  for(int i = 0; i < array->size; i++){
    if(strcmp(tmp[i], "or") == 0)
      num++;
  }
  return num;
}


/*
 * Helper function: takes a wordArray object and iterates through it, checking
 * the errors in syntax from the query. This is written as according to specification
 * in the REQUIREMENTS doc. it returns a TRUE value if there are no "errors" with it;
 * it prints an error message and returns FALSE if there are.
 */
 
bool syntaxCheck(words_t *query){
  char **words = query->words;
  int size = query->size;
  
  if(strcmp(words[0], "and") == 0 || strcmp(words[0], "or") == 0){
    printf("Error: AND/OR operators cannot be in first position\n");
    return false;
  }
  
  if(strcmp(words[size-1], "and") == 0 || strcmp(words[size-1], "or") == 0){
    printf("Error: AND/OR operators cannot be in last position\n");
    return false;
  }
  
  for(int i = 0; i < size - 1; i++){
    if(strcmp(words[i], "and") == 0 || strcmp(words[i], "or") == 0){
      if(strcmp(words[i+1], "and") == 0 || strcmp(words[i+1], "or") == 0){
        printf("Error: AND/OR operators cannot be adjacent\n");
        return false;
      }
    }
  }
  
  return true;
}

/*
 * Helper Function: Takes a string query delimited by spaces, and breaks it
 * into an array of strings, separated at the spaces. Returns NULL on error;
 * if so, does not need to be freed. A successful run returns a pointer to 
 * the allocated array which must be subsequently freed.
 */

 
words_t *parseQuery(char* query, int arraySize){
  if(strcmp(query, "") == 0){
    printf("error: query is empty\n");
    return NULL;
  }
  
  char **array = allocate(sizeof(char*) * arraySize);
  int i = 0;
  char *tmp = strtok(query, " ");
  if(tmp == NULL){
    printf("error: query is empty\n");
    free(array);
    return NULL;
  }
  char *word = allocate(sizeof(char) * strlen(tmp) + 1);
  strcpy(word, tmp);
  NormalizeWord(word);
  
  while(i < arraySize){
    array[i++] = word;
    tmp = strtok(NULL, " ");
    if(tmp == NULL){
        break;
    }
    word = allocate(sizeof(char)*strlen(tmp) + 1);
    strcpy(word, tmp);
    NormalizeWord(word);
  }
  if(i >= arraySize){
    printf("error: query is too long\n");
    free(array);
    return NULL;
  }
  words_t *splitQuery = newWordArray(array, i);
  return splitQuery;
}

/*
 * Helper method to print a prompt and read input from stdin. Return value 
 * will need to be freed later on.
 */
char *askInput(){
  printf("=========================\n");
  printf("Enter Search Query:\n");
  char* input = readlinep(stdin);
  printf("=========================\n");
  return input;
}

/*
 * Helper Method to examine if a string is only alphabetical chars.
 */
 
bool isOnlyAlpha(char* str){
  for(int i = 0; i < strlen(str); i++){
    if(!isalpha(str[i]) && str[i] != ' '){
      return false;
    }
  }
  return true;
}

/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 0 if it exists and isn't empty.
 * Returns 1 if it doesn't exist or is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 * Copied from indexer.c
 */
int directoryIsNonEmpty(char *dirname) {
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(dirname);
  if (dir == NULL) 
    return 1;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 1;
  else
    return 0;
}

docScoreArray_t *newDocScoreArray(int expectedSize){
    docScoreArray_t *tmp = allocate(sizeof(docScoreArray_t));
    tmp->size=0;
    tmp->array = allocate(expectedSize * sizeof(docScore_t*));
    return tmp;
}

/*
 * Useful struct for this module. An docScore_t maps a document number
 * with its score as according to the query. Useful for sorting.
 */

docScore_t *newDocScore(int score, const int docNum){
  docScore_t *tmp = allocate(sizeof(docScore_t));
  tmp->score = score;
  tmp->docNum = docNum;
  return tmp;
}

void addDocToArray(void *array, const int docNum, int docScore){
  docScoreArray_t *tmp = array;
  docScore_t** tmparray = tmp->array;
  docScore_t *item = newDocScore(docScore, docNum);
  tmparray[tmp->size] = item;
  tmp->size++;
}

int compareDocScore(const void *docOne, const void *docTwo)
{
  const docScore_t *first = *(docScore_t **)docOne;
  const docScore_t *second = *(docScore_t **)docTwo;
  int a = first->score;
  int b = second->score;
  if ( a < b ) return 1;
  if ( a == b ) return 0;
  if ( a > b ) return -1;
}
/*
 * Useful struct for this module. An intArray is an array of ints,
 * but with information on its size and the first empty position.
 * Useful for dynamically allocated arrays. Don't forget to call
 * deleteIntArray. Basically an arraylist.
 */

int_t *newIntArray(){
  int_t *tmp = allocate(sizeof(int_t));
  tmp->ints = allocate(sizeof(int) * 200);
  tmp->size = 200;
  tmp->firstEmptyPos = 0;
  return tmp;
}

/*
 * Adds an integer to the end of the intArray. If it hits max size, it expands automatically.
 */

void addIntToArray(int_t *array, int x){
  int_t *tmp = array;
  if(tmp->firstEmptyPos == tmp->size){
    expandArray(array);
  }
  int* ints = tmp->ints;
  ints[array->firstEmptyPos] = x;
  array->firstEmptyPos++;
}

/*
 * Doubles the size of the intArray. Typically used when it hits capacity.
 */
void expandArray(int_t *array){
  int newSize = array->size * 2;
  int* newInts = allocate(sizeof(int) * newSize);
  memcpy(newInts, array->ints, sizeof(int) * array->size);
  free(array->ints);
  array->ints = newInts;
  array->size = newSize;
}

/*
 * Returns value of int at index k.
 */
int getVal(int_t *array, int k){
  return array->ints[k];
}

void setInt(int_t *array, int index, int val){
  array->ints[index] = val;
}

/*
 * Deletes the intArray;
 */

void deleteIntArray(int_t *array){
  free(array->ints);
  free(array);
}
 
/*
 * Useful struct for this module. A WordArray is simply an array of strings,
 * but also contains information on its size; something otherwise difficult 
 * with dynamically allocated arrays. This needs to have deleteWordArray called
 * upon it at some future point.
 */

words_t *newWordArray(char** array, int size){
  words_t *tmp = allocate(sizeof(words_t));
  tmp->words = array;
  tmp->size = size;
  return tmp;
}

/*
 * Helper method: because strtok in parseQuery mutilates the original inputString, 
 * this method outputs the full string stored in the wordArray.
 */

void printWordArray(words_t *array){
  char** tmp = array->words;
  printf("Query:");
  for(int i = 0; i < array->size; i++){
    printf("%s ", tmp[i]);
  }
  printf("\n");
}

/*
 * Fairly straightforward; frees and destroys a given WordArray.
 */

void deleteWordArray(words_t *array){
  char** tmp = array->words;
  while(*tmp){
    free(*tmp);
    ++tmp;
  }
  free(array->words);
  free(array);
}
