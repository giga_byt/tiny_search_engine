The testing for `querier` is contained in the makefile: `make test`
it calls the `make wikitest` in the `indexer` directory, which produces an index
file and a directory of saved webpages in the `indexer` directory. Both of those
are then copied to the `querier` directory, and they are made the parameters for
a `querier` call.