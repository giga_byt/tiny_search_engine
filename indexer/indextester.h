/* 
 * indextester - a test file for the indexer lab
 * usage - ./indexer oldIndexFile newIndexFile
 * the tester writes from the former into the latter.
 * Bill Tang - CS50, August 2017
 */
#define _USAGE "./indexer oldIndexFile newIndexFile"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <webpage.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <ctype.h>
#include <bag.h>
#include <set.h>
#include <hashtable.h>
#include <string.h>
#include "file.h"
#include "counters.h"
#include "index.h"

int main(int argc, char *argv[]);