/* 
 * indexer - a standalone program that reads the document files produced by the TSE crawler, builds an index, and writes that index to a file.
 * usage - ./indexer pageDirectory indexFilename
 * pageDirectory is the name of the folder created by crawler
 * indexFileName is the name of a file into which the index should be writ
 * Assumptions: none yet
 * Bill Tang - CS50, August 2017
 * 
 *
 */
#define _USAGE "./indexer pageDirectory indexFilename"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <webpage.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <bag.h>
#include <set.h>
#include <hashtable.h>
#include <string.h>

/* The main function is called from command-line with command-line args. It makes
 * use of module functions countWords and helper functions directoryIsNonEmpty, 
 * printEntryToFile, and printCountersToFile in its function as it reads through
 * pageDir and scrapes its word counts into an index structure, which is then
 * printed into a file following the format 
 * `word docNum wordCount [docNum wordCount]...`
 * `word2 docNum wordCount [docNum wordCount]...`
 * etc.
 */
int main(int argc, char *argv[]);
 
/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 0 if it exists and isn't empty.
 * Returns 1 if it doesn't exist or is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 * This is modified from the directoryCheck in the crawler program.
 */
 
int directoryIsNonEmpty(char *dirname);

/*
 * Given a webpage, an index, an minimmum word length, and a document number,
 * crawls a webpage (with a certain docNum) for all its words above minLength, 
 * increments the counters in index belonging to the docNum for the words.
 * This is to be meant to be used once for each document, given a certain docNum.
 */

void countWords(webpage_t *page, index_t *wordIndex, int minLength, int docNum);

/*
 * This is a helper function to be passed to index_iterate. on the surface, this takes
 * void*, const char*, void* args, but in reality is meant to take a file output path,
 * the given word (which corresponds to a counters structure in the index), and the 
 * aforementioned counters structure. This passes printCountersToFile to the counters
 * structure's counters_iterate function.
 */
void printEntryToFile(void* fp, const char* word, void *ctrs);

/* This is a helper function to be passed to counters_iterate, in the process of 
 * the above printEntryToFile function. This takes counters and prints them as:
 * `key1 num1 [key2 num2]...` delimited by spaces. 
 */

void printCountersToFile(void *fp, const int key, int count);