/* 
 * indextester - a test file for the indexer lab
 * usage - ./indexer oldIndexFile newIndexFile
 * the tester writes from the former into the latter.
 * Bill Tang - CS50, August 2017
 */
#define _USAGE "./indexer oldIndexFile newIndexFile"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <webpage.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <ctype.h>
#include <bag.h>
#include <set.h>
#include <hashtable.h>
#include <string.h>
#include "file.h"
#include "counters.h"
#include "index.h"


void printEntryToFile(void* fp, const char* word, void *ctrs);
void printCountersToFile(void *fp, const int key, int count);

/* Main Function
 * Does the primary thing.
 */
int main(int argc, char *argv[]){
  
  /**************************Actual Main Function*****************************/
  if(argc != 3){
    printf("Usage:%s\n", _USAGE);
    return 1;
  }
  char *in = argv[1];
  char *out = argv[2];
  FILE *input = fopen(in, "r");
  index_t *dataIndex = index_new(100);
  
  char* str;
  char currWord[80] = "test";
  while((str = readwordp(input))!= NULL){
    if(strcmp(str, "") == 0){
      free(str);
      continue;
    }
    if(atoi(str) == 0){
      memset(currWord, 0, strlen(currWord));
      strcpy(currWord, str); 
      index_initialize_item(dataIndex, currWord);
      free(str);
      continue;
    }
    int countIndex = atoi(str);
    free(str);
    str = readwordp(input);
    int wordCount = atoi(str);
    index_set_count(dataIndex, currWord, countIndex, wordCount);
    free(str);
  }
  fclose(input);
  FILE *output = fopen(out, "w");
  if(output == NULL){
    printf("creation of index failed\n");
    return 1;
  }
  
  index_iterate(dataIndex, output, printEntryToFile);
  
  fclose(output);
  
  index_delete(dataIndex);
}

void printEntryToFile(void* fp, const char* word, void *ctrs){
    FILE *output = fp;
    counters_t *docList = ctrs;
    fprintf(fp, "%-19s ", word);
    counters_iterate(ctrs, fp, printCountersToFile);
    fprintf(fp, "\n");
}
void printCountersToFile(void *fp, const int key, int count){
    FILE *output = fp;
    fprintf(fp, "%d %d ", key, count);
}