# Compilation
To compile, use `make`.
To clean, use `make clean`.
To conduct a memory check valgrind, use `make valgrind`.
To conduct a test, I programmed `wikitest` into the makefile, which will
conduct a test of the indexer/indextest functions on the directory created by
crawler's wikitest.

# Usage
To use the functions, simply type `make all` and then use:
`./indexer pagedir outputfile`
`./indextester inputfile outputfile`