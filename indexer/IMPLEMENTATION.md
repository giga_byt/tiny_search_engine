# Implementation
## Indexer 
### Detailed Pseudo-Code

1. parse args into variables
2. if pagedir exists and contains files; return if fails
3. while pagedir has file "num", num++; num is the number of formatted files in dir
4. initialize index struct
5. for file x = "1" until file "num":
    1. check if file "x" exists
    2. open up file "x"
    3. from known format, make webpage_t from URL, depth, and HTML
    4. don't forget to free the readlinep
    5. close file
    6. check if obtained valid webpage_t; if not, delete webpage, restart loop
    7. if yes, then countWords in the page
        1. while webpage_getNextWord exists,
        2. lowercase the word = getNextWord
        3. if word doesn't already exist in the index, initialize a word counter
        4. increment the word counter
    8. loop
6. create file indexFilename for writing
7. iterate through index with a print function
8. print into file
6. free everything

### indexer.c API

```
/* The main function is called from command-line with command-line args. It makes
 * use of module functions countWords and helper functions directoryIsNonEmpty, 
 * printEntryToFile, and printCountersToFile in its function as it reads through
 * pageDir and scrapes its word counts into an index structure, which is then
 * printed into a file following the format 
 * `word docNum wordCount [docNum wordCount]...`
 * `word2 docNum wordCount [docNum wordCount]...`
 * etc.
 */
int main(int argc, char *argv[]);
 
/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 0 if it exists and isn't empty.
 * Returns 1 if it doesn't exist or is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 * This is modified from the directoryCheck in the crawler program.
 */
 
int directoryIsNonEmpty(char *dirname);

/*
 * Given a webpage, an index, an minimmum word length, and a document number,
 * crawls a webpage (with a certain docNum) for all its words above minLength, 
 * increments the counters in index belonging to the docNum for the words.
 * This is to be meant to be used once for each document, given a certain docNum.
 */

void countWords(webpage_t *page, index_t *wordIndex, int minLength, int docNum);

/*
 * This is a helper function to be passed to index_iterate. on the surface, this takes
 * void*, const char*, void* args, but in reality is meant to take a file output path,
 * the given word (which corresponds to a counters structure in the index), and the 
 * aforementioned counters structure. This passes printCountersToFile to the counters
 * structure's counters_iterate function.
 */
void printEntryToFile(void* fp, const char* word, void *ctrs);

/* This is a helper function to be passed to counters_iterate, in the process of 
 * the above printEntryToFile function. This takes counters and prints them as:
 * `key1 num1 [key2 num2]...` delimited by spaces. 
 */

void printCountersToFile(void *fp, const int key, int count);
```
### Data Structures
indexer.c features one prominent data struct: wordIndex, which is an index struct
that maps `"word"` to `(docNum, count)` pairs. The index is simply an organized way
to store the word data that is read inwards. Later, the data is written from the index
into a file following the aforementioned format.

### Error Handling
The program catches command-line errors of all types, bad parameter errors resulting
from bad arguments, and almost all user input failures including bad dirs. It also
error-catches if it attempts to open a file that doesn't exist. 

It does NOT error-check the format of the file that it opens, and it will attempt
to read empty files into a `webpage_t` object. However, it does errorcheck the 
contents of webpage_t to ensure that the wordcounter is being passed a correctly
made webpage. 

### Resource Management
All things are malloc'ed and freed that need to be. The index is malloc'ed, and 
freed afterwards. Every readlinep and readwordp is freed as well, and every `fopen`,
there is an `fclose`.

### Persistent Storage
The output of the file is stored in the supplied file as given by the indexFilename.

## Indextester
### Detailed Pseudo-Code

1. parse args into variables
2. open the input file
3. initialize index struct to `dataIndex`
5. while str = readwordp from input,
    1. ensure str is not empty or NULL (indicating `\n` or `EOF`) otherwise free and loop
    2. if str is a word, set `currentword` to str and free and loop
    3. if str can be parsed as an int, parse this str into countIndex, free str,
    4. readwordp again, and parse *that* str into wordCount, free str
    5. set the `dataIndex` counter with key `currentword`, `countindex`, to `wordCount`
    6. loop
6. close input
6. create file outut for writing
7. iterate through index with a print function
8. print into file
6. free everything

### indextester.c API

```
/* The main function is called from command-line with command-line args. It makes
 * use of helper functions printEntryToFile, and printCountersToFile in its 
 * function as it parses the input of the format:
 * `word docNum wordCount [docNum wordCount]...`
 * `word2 docNum wordCount [docNum wordCount]...`
 * etc.
 * into index. Having recreated an index from input, it then prints it out in the same
 * format into the output file.
 */
int main(int argc, char *argv[]);

/*
 * This is a helper function to be passed to index_iterate. on the surface, this takes
 * void*, const char*, void* args, but in reality is meant to take a file output path,
 * the given word (which corresponds to a counters structure in the index), and the 
 * aforementioned counters structure. This passes printCountersToFile to the counters
 * structure's counters_iterate function.
 */
void printEntryToFile(void* fp, const char* word, void *ctrs);

/* This is a helper function to be passed to counters_iterate, in the process of 
 * the above printEntryToFile function. This takes counters and prints them as:
 * `key1 num1 [key2 num2]...` delimited by spaces. 
 */

void printCountersToFile(void *fp, const int key, int count);
```
### Data Structures
indextester.c features one prominent data struct: dataIndex, which is an index struct
that maps `"word"` to `(docNum, count)` pairs. The goal of indextester is to attempt
to repopulate dataIndex from a file containing "index data".
Later, the data is written from the index into a file following the same format.

### Error Handling
Because of the assumptions permitted in the REQUIREMENTS spec, indextester has
almost no error-checking. It assumes that input is in proper format as given above.

It does errorcheck when reading the input, however, catching EOFs and empty strings,
and as a result will not critically fail if given an empty file. There are few other
opportunities to catch errors.

### Resource Management
All things are malloc'ed and freed that need to be. The index is malloc'ed, and 
freed afterwards. Every readwordp is freed as well, and every `fopen`,
there is an `fclose`.

### Persistent Storage
The output of the file is stored in the supplied file as given by the output.