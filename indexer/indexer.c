/* 
 * indexer - a standalone program that reads the document files produced by the TSE crawler, builds an index, and writes that index to a file.
 * usage - ./indexer pageDirectory indexFilename
 * pageDirectory is the name of the folder created by crawler
 * indexFileName is the name of a file into which the index should be writ
 * Assumptions: none yet
 * Bill Tang - CS50, August 2017
 * 
 *
 */
#define _USAGE "./indexer pageDirectory indexFilename"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <webpage.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <ctype.h>
#include <bag.h>
#include <set.h>
#include <hashtable.h>
#include <string.h>
#include "file.h"
#include "counters.h"
#include "index.h"

int directoryIsNonEmpty(char *dirname);
void delString(void* str);
void countWords(webpage_t *page, index_t *wordIndex, int minLength, int docNum);
void printEntryToFile(void* fp, const char* word, void *ctrs);
void printCountersToFile(void *fp, const int key, int count);

/* Main Function
 * Does the primary thing.
 */
int main(int argc, char *argv[]){
  const int minLength = 2;
  
  /**************************Actual Main Function*****************************/
  if(argc != 3){
    printf("Usage:%s\n", _USAGE);
    return 1;
  }
  
  char *pageDir = argv[1];
  char *fileName = argv[2];
  
  if(directoryIsNonEmpty(pageDir) != 0){
    printf("Usage:%s || PageDir must contain files and exist\n", _USAGE);
    return 1;
  }
  
  //Calculates the number of HTML copies that we've saved.
  char cacheFileName[80];
  int numCacheFiles = 0;
  sprintf(cacheFileName, "%s/%04d.txt", pageDir, numCacheFiles + 1);
  while(access(cacheFileName, F_OK) != -1){
    numCacheFiles++;
    memset(cacheFileName,0,strlen(cacheFileName));
    sprintf(cacheFileName, "%s/%04d.txt", pageDir, numCacheFiles + 1);
  }
  memset(cacheFileName,0,strlen(cacheFileName));
  //create our index
  index_t *wordIndex = index_new(numCacheFiles * 2);
  
  for(int i = 0; i < numCacheFiles; ++i){

    sprintf(cacheFileName, "%s/%04d.txt", pageDir, i + 1);
    if(access( cacheFileName, F_OK ) == -1){
      continue;
    }
    
    FILE *tmp = fopen(cacheFileName, "r");

    char* url = readlinep(tmp);
    char* num = readlinep(tmp);
    int depth = atoi(num);
    char* html = readfilep(tmp);
    bool validFile = false;
    if(url && num && html){
      validFile = true;
    }
    webpage_t *curr = webpage_new(url, depth, html);
    free(url);
    free(num);
    fclose(tmp);

    if(validFile){
      countWords(curr, wordIndex, minLength, i+1);
    }
    
    memset(cacheFileName,0,strlen(cacheFileName));
    webpage_delete(curr);
  }
  
  FILE *indexFile = fopen(fileName, "w");
  if(indexFile == NULL){
    printf("creation of index failed\n");
    return 1;
  }
  
  index_iterate(wordIndex, indexFile, printEntryToFile);
  
  fclose(indexFile);
  
  //FREED
  index_delete(wordIndex);
}
 

/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 0 if it exists and isn't empty.
 * Returns 1 if it doesn't exist or is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 * This is modified from the directoryCheck in the crawler program.
 */
int directoryIsNonEmpty(char *dirname) {
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(dirname);
  if (dir == NULL) 
    return 1;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 1;
  else
    return 0;
}

/*
 * Deletion method for wordList, a set of Strings.
 *
 */

void delString(void* str){
    char* tmp = str;
    free(str);
}

/*
 * Given a webpage, a counter, and a list of "words seen,"
 * crawls a webpage for all its words, increments counter appropriately,
 * and updates the list of "words seen".
 */

void countWords(webpage_t *page, index_t *wordIndex, int minLength, int docNum){
  int pos = 0;
  char *result;
  while ((pos = webpage_getNextWord(page, pos, &result)) > 0) {
    if(strlen(result) > minLength){
      //"normalize by lowercasing"
      int i = 0;
      while( result[i] ){
        result[i] = (char)tolower(result[i]);
        i++;
      }
      //check if exists; if not, initialize, if so, do nothing.
      if(!index_find_key(wordIndex, result)){
        index_initialize_item(wordIndex, result);
      }
      //add to counters
      index_increment(wordIndex, result, docNum);
    }free(result);
  }
}

void printEntryToFile(void* fp, const char* word, void *ctrs){
    FILE *output = fp;
    counters_t *docList = ctrs;
    fprintf(fp, "%-19s ", word);
    counters_iterate(ctrs, fp, printCountersToFile);
    fprintf(fp, "\n");
}

void printCountersToFile(void *fp, const int key, int count){
    FILE *output = fp;
    fprintf(fp, "%d %d ", key, count);
}
/************************TEST FUNCS**********************************/