# TESTING
The testing for both indexer and indextest are contained in the makefile.
`make wikitest` is described in the makefile, but this runs `indexer` on the output
of `crawler` on the Wikipedia test mode, and then runs `indextester` on the output
of `indexer`; finally, sorts their outputs, and compares the two.

Ideally, nothing is echoed or printed, and indeeed this is the case, as 
the two files are identical in every way.