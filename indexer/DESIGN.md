# Indexer
## User Interface
The indexer’s only interface with the user is on the command-line; 
it must always have two arguments.
`indexer pageDir outputFile`
For example:
`./indexer testDir index`
## Inputs and Outputs
Input: the only inputs are command-line parameters; see the User Interface above.
Output: a single file containing the inverse index-structure translated to text form.

## Functional decomposition into modules
1. main, which parses arguments and initializes other modules
2. indexer, which loads input files, processes them, and writes them into an index struct
3. indexsaver, which iterates through the finished index and prints the structure into a file
4. And some helper modules that provide data structures:

index, which itself uses a great deal of hashtables, sets, etc.

## Pseudocode
The indexer runs as follows:
1. read in args
2. calculate the amount of files to be read as input
3. initialize the index
4. iterates through all files in the pagedir
    1. load a file
    2. load the information in the file into a webpage struct
    3. parse the webpage for words
        1. gets a word
        2. lowercase it
        3. if not already in the index, add to index
        4. increment the word-entry in the index
5. finally, passes a print method to index which prints the whole index to file.

## Dataflow through modules
1. main parses parameters and passes them to the indexer.
2. indexer takes these args and processes them all into index, which is given to the indexsaver.
3. indexsaver takes the index and saves it into a file

## Major Data Structures
One helper module is used, the index, a hashtable mapping *words* to *(docNum, count)*.

## Testing plan
*Unit Testing.* Each individual module in it was tested on a junk dir and junk 
files, which no longer exist, and confirmed to work.

*Integration Testing.* Tested the program with various bad command-line args,
bad dirs, non-existent files, which can be done simply via command line. Finally,
it is tested on actual output from the crawler, in the makefile.

# Indextester
## User Interface
The indexer’s only interface with the user is on the command-line; 
it must always have two arguments.
`indextester input output`
For example:
`./indexer index indexcopy`

## Inputs and Outputs
Input: the only inputs are command-line parameters, one of which points to an index file.
Output: a file that should be the copy of the input file

## Functional decomposition into modules
1. main, which parses arguments and initializes other modules
2. reader, which loads in the index file, and writes it into an index struct
3. writer, which iterates through the finished index and prints the structure into a file
4. And some helper modules that provide data structures:

index, which itself uses a great deal of hashtables, sets, etc.

## Pseudocode
The indexer runs as follows:
1. read in args
2. initialize the index
3. load the indexfile
4. parse word-by-word, inserting (docnum, count) pairs into the corresponding word slots.
5. finally, passes a print method to index which prints the whole index to file.

## Dataflow through modules
1. main parses parameters and passes them to the indexer.
2. reader takes the file arg from main and loads it into an index which is given to the indexsaver.
3. `writer` takes the index and saves it into a file

## Major Data Structures
One helper module is used, the index, a hashtable mapping *words* to *(docNum, count)*.

## Testing plan
*Unit Testing.* Each individual module in it was tested on a junk dir and junk 
files, which no longer exist, and confirmed to work.

*Integration Testing.* Tested the program with various bad command-line args,
non-existent files, which can be done simply via command line. Finally,
it is tested on actual output from the indexer, in the makefile.

