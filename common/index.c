/* 
 * index.c - CS50 'index' module 
 *
 * see index.h for more information
 *
 * Bill Tang, July 2017
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "index.h"
#include "memory.h"
#include "jhash.h" 
#include "set.h"
#include "counters.h"
#include "file.h"


/**************** global types ****************/
typedef struct index {
  int size;         //Tracks number of slots
  set_t **table; //the array of slots themselves
} index_t;

/* Create a new (empty) index; return NULL if error. 
 * 
 **/
index_t *index_new(const int num_slots) {
  index_t *tmp = allocate(sizeof(index_t)); //Allocate index
  tmp->size = num_slots;                            //fill out size var
      //allocate the actual table
  tmp->table = allocate(sizeof(set_t*) * num_slots);
  for(int i = 0; i < num_slots; ++i){
    tmp->table[i] = set_new();
  }
  return tmp;
}


/*
 * Initialize the counter for the slot identified by key (string).
 * The key string is copied for use by the index.
 * Returns false if it has already been initialized, any paramater 
 * is NULL, or error: return true iff new counter was init.
 */
bool index_initialize_item(index_t *ind, const char *key){
  if(ind && key){
    const int mod = ind->size;
    unsigned long hash = JenkinsHash(key, mod);
    //get ref to hashed slot-set
    set_t *curr = ind->table[hash];
    if(set_find(curr, key))
      return false;
    counters_t* keyCounter = counters_new();
    set_insert(curr, key, keyCounter);
    
    if(set_find(curr, key))
      return true;
    else
      return false;
  } return false;
}

/*
 * Searches the index's hashtable for key (string). Returns true on exists,
 * false on key not existing in the hashtable.
 * This complements index_initialize_item.
 */
void *index_find_key(index_t *ind, const char *key){
  if(ind && key){
    const int mod = ind->size;
    unsigned long hash = JenkinsHash(key, mod);
    //get ref to hashed slot-set
    set_t *curr = ind->table[hash];
    
    if(set_find(curr, key)){
      return set_find(curr, key);
    }
    else return NULL;
  } return NULL;
}

/* 
 * Returns the number contained in index counterIndex (int) which is associated with
 * key (string). Returns 0 on any failure.
 */
int index_get_count(index_t *ind, const char *key, const int counterIndex) {
  //check for existence of both
  if(ind && key && counterIndex >= 0){
     //get hash value for key
    const int mod = ind->size;
    unsigned long hash = JenkinsHash(key, mod);
    set_t *curr = ind->table[hash];

    if(set_find(curr, key)){
      counters_t *currCounter = set_find(curr, key);
      return counters_get(currCounter, counterIndex);
    }
    //key doesn't exist
    return 0;
  } else {
  return 0;
  }
}

int index_increment(index_t *ind, const char *key, const int counterIndex){
  if(ind && key && counterIndex >= 0){
     //get hash value for key
    const int mod = ind->size;
    unsigned long hash = JenkinsHash(key, mod);
    set_t *curr = ind->table[hash];
    
    if(set_find(curr, key)){
      counters_t *currCounter = set_find(curr, key);
      counters_add(currCounter, counterIndex);
      return counters_get(currCounter, counterIndex);
    }
    //key doesn't exist
    return 0;
  } else {
  return 0;
  }
}

int index_set_count(index_t *ind, const char *key, 
    const int counterIndex, int count){
  if(ind && key && counterIndex >= 0 && count >=0 ){
     //get hash value for key
    const int mod = ind->size;
    unsigned long hash = JenkinsHash(key, mod);
    set_t *curr = ind->table[hash];
    
    if(set_find(curr, key)){
      counters_t *currCounter = set_find(curr, key);
      counters_set(currCounter, counterIndex, count);
      return counters_get(currCounter, counterIndex);
    }
    //key doesn't exist
    return 0;
  } else {
  return 0;
  }
}

/* Iterate over all items in the table; in undefined order.
 * Call the given function on each item, with (arg, key, item).
 * If ind==NULL or itemfunc==NULL, do nothing.
 */
void index_iterate(index_t *ind, void *arg,
               void (*itemfunc)(void *arg, const char *key, void *item) ){
  if(ind && itemfunc){
    //call itemfunc on each item in order
    for(int x = 0; x < ind->size; ++x){
      set_t *curr = ind->table[x];    
      set_iterate(curr, arg, itemfunc); 
    }
  }
}

/*
 * A method of passing counters_delete to set_delete
 */

void delete_counters(void* ctrs){
    counters_t *curr = ctrs;
    counters_delete(curr);
}
/* Delete the whole index; ignore NULL ind.
 */
void index_delete(index_t *ind ) {
  if(ind){
    for(int x = 0; x < ind->size; ++x){
      //iterate and delete all items
      set_t *curr = ind->table[x];    
      set_delete(curr, delete_counters);
    }
      //destroy table
    count_free(ind->table);
      //destroy entire index
    count_free(ind);
  }
}

index_t *load_index_from_file(char* fileName, int numSlots){
  index_t *tmp = index_new(numSlots);
  FILE *input = fopen(fileName, "r");
  
  char* str;
  char currWord[160] = "test";
  while((str = readwordp(input))!= NULL){
    if(strcmp(str, "") == 0){
      free(str);
      continue;
    }
    if(atoi(str) == 0){
      memset(currWord, 0, strlen(currWord));
      strcpy(currWord, str); 
      index_initialize_item(tmp, currWord);
      free(str);
      continue;
    }
    int countIndex = atoi(str);
    free(str);
    str = readwordp(input);
    int wordCount = atoi(str);
    index_set_count(tmp, currWord, countIndex, wordCount);
    free(str);
  }
  fclose(input);
  return tmp;
}