/* 
 * index.h - CS50 'index' module 
 *
 *
 * Bill Tang, August 2017
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>




/**************** global types ****************/
typedef struct index index_t;

/* 
 * Create a new (empty) index; return NULL if error. 
 * 
 */
index_t *index_new(const int num_slots);


/*
 * Initialize the counter for the slot identified by key (string).
 * The key string is copied for use by the index.
 * Returns false if it has already been initialized, any paramater 
 * is NULL, or error: return true iff new counter was init.
 */
bool index_initialize_item(index_t *ind, const char *key);

/* 
 * Returns the number contained in index counterIndex (int) which is associated with
 * key (string). Returns NULL on any failure.
 */
int index_get_count(index_t *ind, const char *key, const int counterIndex);

/*
 * Sets the value of the index associated with counterIndex (int), itself in
 * the counters field associated with key (string).
 */

int index_set_count(index_t *ind, const char *key, const int counterIndex, int count);

/*
 * Searches the index's hashtable for key (string). Returns item on exists,
 * NULL on key not existing in the hashtable.
 * This complements index_initialize_item.
 */
void *index_find_key(index_t *ind, const char *key);

/*
 * Increments the number contained in index counterIndex (int) which is associated
 * with key (string). This  contains all error-checking necessary to handle cases
 * such as counterIndex not existing; however, it is necessary to first initialize
 * the counters for key (string)
 */
int index_increment(index_t *ind, const char *key, const int counterIndex);

/* Iterate over all items in the table; in undefined order.
 * Call the given function on each item, with (arg, key, item).
 * If ind==NULL or itemfunc==NULL, do nothing.
 */
void index_iterate(index_t *ind, void *arg,
               void (*itemfunc)(void *arg, const char *key, void *item) );
/* Delete the whole index; ignore NULL ind.
 * No input from the user is required except the index
 * to be deleted.
 */
void index_delete(index_t *ind);


/*
 * Given a file in the format of the established index format file,
 * this function reads the file into an index struct and returns a pointer 
 * to said index.
 */
index_t *load_index_from_file(char* fileName, int numSlots);