/* 
 * crawler - the thing that crawls everything - run this.
 * usage - crawler seedURL pageDirectory maxDepth
 * Assumption: pageDirectory does NOT end in '/'.
 * Bill Tang - CS50, July 2017
 * 
 *
 */

#define _USAGE "crawler seedURL pageDirectory maxDepth"

#include <stdbool.h>
#include <webpage.h>

/***********************************************************************/

//Main Function

int main(int argc, char *argv[]);


/*
 * Helper method to fetch a page given by *page.
 */
bool pageFetch(webpage_t *page);

/*
 * Helper method to save a page to a file
 */
void pageSave(webpage_t *page, int count, char *pageDir);


/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 1 if it doesn't exist or is non-empty.
 * Returns 0 if it exists and is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 */
int directoryCheck(char *dirname);

/************************UNIT TESTS**********************************/

void testDirectoryCheck();

void testPageSave();