# Compilation
To compile, use `make`.
To clean, use `make clean`.
To conduct a memory check valgrind, use `make valgrind`.

A variety of testing options exist - these are as follows:

1. `make wikitest`: runs a maxdepth 1 crawl on http://old-www.cs.dartmouth.edu/~cs50/data/tse/wikipedia/.
2. `make cs50test`: runs a maxdepth 2 crawl on http://old-www.cs.dartmouth.edu/~cs50/index.html.
3. `make faketest`: runs a crawl on a URL that doesn't exist.
4. `make baddirtest`: attempts to run a crawl without a corresponding directory.

**A few important notes:** some of these tests will cause a "recipe failed" error,
primarily due to the fact that the commands themselves are designed to be flawed
(baddirtest, for example) and thus inherently cause the recipe to fail. Curiously,
valgrind also does this, despite naked `valgrind crawler` and its various options
resulting in no such error.

# Usage
To use the crawler, simply `make` and then follow the usage:
`./crawler seedURL pageDir maxDepth`