/* 
 * crawler - the thing that crawls everything - run this.
 * usage - crawler seedURL pageDirectory maxDepth
 * Assumption: pageDirectory does NOT end in '/'.
 * Bill Tang - CS50, July 2017
 * 
 *
 */
#define _USAGE "crawler seedURL pageDirectory maxDepth"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <webpage.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <bag.h>
#include <set.h>
#include <hashtable.h>
#include <string.h>

int directoryCheck(char *dirname);
bool pageFetch(webpage_t *page);
void pageSave(webpage_t *page, int count, char *pageDir);
void testDirectoryCheck();
void testPageSave();
void hashPrintURLs(FILE *arg, const char *key, void *item);
void crawlURL(webpage_t *currPage, hashtable_t *URLTable, bag_t *URLQueue);

/***********************************************************************/
//Main Function
int main(int argc, char *argv[]){
  const int maxDepthFinal = 20;
  /*******************************Unit Tests*********************************/
  /*
  hashtable_t *URLTable = hashtable_new(1000);
  bag_t *URLQueue = bag_new();
  testDirectoryCheck();
  testPageSave();
  */
  /**************************Actual Main Function*****************************/
  if(argc != 4){
    printf("Usage:%s\n", _USAGE);
    return 1;
  }
  
  char *seedURL = argv[1];
  char *pageDir = argv[2];
  int maxDepth;
  
  if(directoryCheck(pageDir) == 1){
    printf("Usage:%s || PageDir must be empty!\n", _USAGE);
    return 1;
  }
  
  int pageCount = 0;
  
  if((strcmp(argv[3], "0")) || atoi(argv[3]) != 0 ){
    maxDepth = atoi(argv[3]);
    if(maxDepth < 0){
      printf("Need positive maxDepth\n");
      return 1;
    }
  } else {
    printf("Usage:%s\n", _USAGE);
    return 1;
  }
  
  if(maxDepth > maxDepthFinal){
      maxDepth = maxDepthFinal;
  }
  hashtable_t *URLTable = hashtable_new(10000);
  bag_t *URLQueue = bag_new();
  webpage_t *seedPage = webpage_new(seedURL, 0, NULL);
  webpage_t *seedCopy = webpage_new(seedURL, 0, NULL);
  bag_insert(URLQueue, seedPage);
  hashtable_insert(URLTable, seedURL, seedCopy);
  webpage_t *currPage;
  while((currPage = bag_extract(URLQueue))){ 
    if(pageFetch(currPage)){
      //increment pageCount and save
      pageCount++;
      pageSave(currPage, pageCount, pageDir);
      
      //if we're not at maximum depth, add all links we can find
      //this is taken from the usage example in webpage.h
      int currDepth = webpage_getDepth(currPage);
      if(currDepth < maxDepth){
        crawlURL(currPage, URLTable, URLQueue);
      }
    }
    webpage_delete(currPage);
  }
  hashtable_delete(URLTable, webpage_delete);
  bag_delete(URLQueue, webpage_delete);
}



/*
 * this function takes the current page, a hashtable of seen URLs, and
 * the queue of URLs to be crawled, and puts all previously unseen URLS
 * in the current page into the URLQueue so long as the URLs conform to
 * IsInternalURL.
 * This is primarily created to clean up the code.
 */
 
void crawlURL(webpage_t *currPage, hashtable_t *URLTable, bag_t *URLQueue){
  char *nextURL;
  int pos = 0;
  while((pos = webpage_getNextURL(currPage, pos, &nextURL)) > 0){
    NormalizeURL(nextURL);
    if(IsInternalURL(nextURL)){
      if(hashtable_find(URLTable, nextURL) == NULL){
        hashtable_insert(URLTable, nextURL, 
            webpage_new(nextURL, webpage_getDepth(currPage) + 1, NULL));
        bag_insert(URLQueue, webpage_new(nextURL, 
              webpage_getDepth(currPage) + 1, NULL));
        printf("%s\n", nextURL);
      }
    }
    free(nextURL);
  }
}


/*
 * Helper method to fetch a page given by *page. It fetches, and error-checks
 * the fetch automatically.
 */
bool pageFetch(webpage_t *page) {
  if(webpage_fetch(page)){
    return true;
  } else {
    printf("Pagefetch failed for URL: %s\n", webpage_getURL(page));
    return false;
  }
}


/*
 * Helper method to save a page to a file. This takes a webpage, an ID number,
 * and the directory in which the file is to be saved, then creates and fills
 * the text file with first the URL, then the Depth, then the full HTML.
 */
void pageSave(webpage_t *page, int count, char *pageDir) {
  if(page){
    char str[80];
    sprintf(str, "%s/%04d.txt", pageDir, count);
    FILE *temp = fopen(str, "w");
    
    if(temp == NULL){
      printf("Failed to Create File %s\n", str);
    }
    
    char *URL = webpage_getURL(page);
    char *HTML = webpage_getHTML(page);
    
    if(!URL){
      fprintf(temp, "Error: URL is NULL\n");
      return;
    } if(!HTML) {
      fprintf(temp,"Error: HTML does not exist for given URL.\n");
      return;
    }
    fputs(URL, temp);
    fputs("\n", temp);
    fprintf(temp, "%d\n", webpage_getDepth(page));
    fputs(webpage_getHTML(page), temp);
    fclose(temp);
    printf("\t\t\t%s Saved to: %s\n", URL, str);
  }
}


/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 1 if it doesn't exist or is non-empty.
 * Returns 0 if it exists and is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 */
int directoryCheck(char *dirname) {
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(dirname);
  if (dir == NULL) 
    return 1;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 0;
  else
    return 1;
}

/************************TEST FUNCS**********************************/

/*
 * These unit test functions are built to test a specific helper function,
 * ensuring that they do their job, or built to aid in debugging (for example,
 * the function that provides a print function for the Hashtable).
 */
 
void testDirectoryCheck(){
  printf("%d\n", directoryCheck("testDir/"));
  printf("this should print 0\n");
  printf("%d\n", directoryCheck("fakeDir/"));
  printf("this should print 1\n");
}
void testPageSave(){
  webpage_t *test = webpage_new("http://www.cs.dartmouth.edu/"
          "~cs50/Labs/Lab4/DESIGN.html", 12, NULL);
  pageFetch(test);
  pageSave(test, 2, "testDir");
}

void hashPrintURLs(FILE *arg, const char *key, void *item){
    FILE *fp = arg;
    webpage_t *page;
    if(fp != NULL && key && item){
        page = item;
        fprintf(fp, "\t(Key is contained:%s)\n", webpage_getURL(page));
    }
}
