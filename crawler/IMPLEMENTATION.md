# Implementation

## Detailed Pseudo-Code

1. parse args into variables
2. check pageDir for existence and emptiness; return if fails
3. initialize hashtable URLTable and bag URLQueue
4. initialize the seed webpage, copy into both
5. while(URLQueue not empty, get current page)
    1. if page's HTML can be fetched
    2. fetch
    3. save the page
        1. save URL
        2. save depth
        3. save fetched HTML
    4. if depth of current page is not maxed
    5. crawl page for URLS
        1. get the next url
        2. normalize it
        3. check for internal status
        4. compare to URLTable - has it been seen?
        5. if not, then insert into URL table, insert into URL Queue
    6. loop
6. free everything

## Crawler.c API

```
/*
 * The main function is called from the command line with command-line args.
 * It makes use of crawlURL, pageFetch, pageSave, and directoryCheck in its
 * function as it creates a set of files in the given directory, which are copies
 * of html files (with auxiliary information) crawled from the seedURL to a certain
 * maxDepth.
 */
int main(int argc, char *argv[]);

/*
 * this function takes the current page, a hashtable of seen URLs, and
 * the queue of URLs to be crawled, and puts all previously unseen URLS
 * in the current page into the URLQueue so long as the URLs conform to
 * IsInternalURL.
 */
void crawlURL(webpage_t *currPage, hashtable_t *URLTable, bag_t *URLQueue);


/*
 * Helper method to fetch a page given by *page. It fetches, and error-checks
 * the fetch automatically.
 */
bool pageFetch(webpage_t *page);

/*
 * Helper method to save a page to a file. This takes a webpage, an ID number,
 * and the directory in which the file is to be saved, then creates and fills
 * the text file with first the URL, then the Depth, then the full HTML.
 */
void pageSave(webpage_t *page, int count, char *pageDir);

/*
 * Helper method to examine a directory given by *dirname.
 *
 * Returns 1 if it doesn't exist or is non-empty.
 * Returns 0 if it exists and is empty.
 *
 * This was taken from a question on Stack Exchange, credit to username "freethinker".
 */
int directoryCheck(char *dirname);

/************************TEST FUNCS**********************************/
/*
 * These unit test functions are built to test a specific helper function,
 * ensuring that they do their job, or built to aid in debugging (for example,
 * the function that provides a print function for the Hashtable).
 */
void testDirectoryCheck()
void testPageSave()
void hashPrintURLs(FILE *arg, const char *key, void *item)
```
## Data Structures
Crawler.c features two prominently: `URLQueue`, a `bag`, and `URLTable`, a `hashtable`.
Both contain `webpage_t` items. `URLQueue`, as is named, is meant to be a queue into 
which `webpage_t` items are pulled out of and acted upon. `URLTable` is a running
list of URLs already seen, and is checked against to make sure that no double-scraping
occurs.

## Error Handling
The program catches command-line errors of all types, bad parameter errors resulting
from bad arguments, and almost all user input failures include bad dirs. In its 
normal function, it also catches bad webpage scraping, logging when it fails to 
retrieve an html for a given URL, and I/O errors involving saving the pages to files.

## Resource Management
All things are malloc'ed and freed that need to be. There are no possible leaks of 
any kind.

## Persistent Storage
The output of the file is stored in the supplied directory as text files containing
the information listed above. 