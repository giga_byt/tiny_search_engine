I went over the majority of this in the README.md. I included a variety of testing
options in the makefile, all listed in the README. Their outputs are very standard.
The crawler itself prints out each URL that is added to the bag, as well as printing
it again when it's saved to directory to ensure no wonky behavior or double-saving.